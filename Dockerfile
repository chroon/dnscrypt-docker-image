# build stage
FROM golang:1.13.8 AS builder

ENV DNSCRYPTVERSION=2.0.39
ENV DNSCRYPTGITURL=https://github.com/jedisct1/dnscrypt-proxy
ENV PRJPATH=$GOPATH/src/dnscrypt-proxy

WORKDIR /tmp

RUN curl https://raw.githubusercontent.com/golang/dep/master/install.sh | sh

RUN git clone ${DNSCRYPTGITURL} src && \
    cd src && \
    git checkout tags/${DNSCRYPTVERSION} -b ${DNSCRYPTVERSION}-branch && \
    mkdir ${PRJPATH} && cp -r dnscrypt-proxy/* ${PRJPATH}

WORKDIR ${PRJPATH}

RUN dep init && \
    dep ensure

RUN CGO_ENABLED=0 go install -a -ldflags '-s -w -extldflags "-static"'

# container stage
FROM alpine:3.11.3

LABEL maintainer="Daniel Scheidegger <daniel@scheidegger.ag>"

COPY --from=builder /go/bin/dnscrypt-proxy /dnscrypt-proxy
COPY /config/dnscrypt-proxy.toml /dnscrypt-proxy.toml

ENTRYPOINT ["/dnscrypt-proxy",  "-config", "dnscrypt-proxy.toml"]

HEALTHCHECK --interval=5m --timeout=5s --start-period=1m --retries=3 \
    CMD ["/dnscrypt-proxy", "-resolve",  "google.com"] || exit 1